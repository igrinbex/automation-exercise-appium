# Automation Exercise – Appium 

## Technologies/Frameworks Used

### Technologies Used

- Java 1.8
- Appium
- Cucumber/Gherkin
- TestNG
- Junit
- Maven

### Frameworks Used

- Page Object Model with Page Factory

### Prerequisites

1. JDK 8
   a. `brew cask install java`
   b. `brew install maven`
   c. `brew tap adoptopenjdk/openjdk`
   d. `brew cask install adoptopenjdk8`
   e. `export JAVA_HOME=/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/` Home needs to be added to .bash_profile

2. Maven
   a. `brew update`
   b. `brew install maven`
   c. `export M2_HOME=/usr/local/Cellar/maven/3.5.0/libexec`
   d. `export M2=${M2_HOME}/bin`
   e. `export PATH=${PATH}:${M2_HOME}/bin`

3. Appium server
   a. `npm install -g appium`
   b. `npm install -g appium-doctor`



#### Properties

Create a file locally at `/src/test/resources/env.properties` to store the following environment variables. This will not be saved in source control. Each line should take the format of `key=value`.

- **driver.path** _(required)_: path to web drivers (see [Downloading WebDrivers](#downloading-webdrivers))
- **test.browser**: passing a value of `headless` will configure a headless run


## Setup
1. Running Appium server
2. Connected device (I use Android Emulator ver. 10 )
3. env.propertu configuration
## Run
1. Maven command line: `mvn clean test`

## Automation test coverage

1. Chrome - Google test case
2. Application - AOL test case

## Report an images
1. Report will be saved /target/Output/
2. Images
    a. Fail - 
    b. Email verification