package runner;


import com.cucumber.listener.ExtentCucumberFormatter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.BeforeClass;

import java.io.File;

@CucumberOptions(
        plugin = {"json:target/cucumber-reports/Cucumber.json",
                "junit:target/cucumber-reports/Cucumber.xml",
                "html:target/cucumber-reports",
                "pretty", "html:target/Runner",
                "com.cucumber.listener.ExtentCucumberFormatter"},
        features = {"src/test/resources/features"},
        tags = {"~@Ignore", "@App"},
        glue = {"stepdefs"},
        monochrome = true
)
public class RunnerApp extends AbstractTestNGCucumberTests {
    @BeforeClass
    public static void setup() {
        String fileName = "target/Output/index.html";
        File newFile = new File(fileName);
        ExtentCucumberFormatter.initiateExtentCucumberFormatter(newFile, false);
        ExtentCucumberFormatter.loadConfig(new File(System.getProperty("user.dir") + "/src/test/resources/extent-config.xml"));
    }

}
