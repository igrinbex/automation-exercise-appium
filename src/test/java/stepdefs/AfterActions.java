package stepdefs;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import utils.BaseClass;
import utils.BaseClassWeb;

public class AfterActions {
    @After

    public static void tearDown(Scenario scenario) {
        String env = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("env");

        if (env.equals("Chrome")) {
            WebDriver driver = BaseClassWeb.getDriver();

            System.out.println(scenario.isFailed());
            if (scenario.isFailed()) {
                byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshotBytes, "image/png");
            }
            BaseClass.tearDown();
        } else {
            WebDriver driver = BaseClass.getDriver();

            System.out.println(scenario.isFailed());
            if (scenario.isFailed()) {
                byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshotBytes, "image/png");
            }
            BaseClass.tearDown();
        }
    }
}
