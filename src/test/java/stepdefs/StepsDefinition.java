package stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.asserts.SoftAssert;
import pages.actions.AolPageActions;
import pages.actions.GooglePageActions;
import utils.BaseClass;
import utils.BaseClassWeb;

import java.io.File;
import java.io.IOException;

public class StepsDefinition {
    private GooglePageActions googlePageActions = new GooglePageActions();
    private AolPageActions aolPageActions = new AolPageActions();
    private SoftAssert softAssertion = new SoftAssert();

    @Given("^I navigate to \"([^\"]*)\"$")
    public void iNavigateTo(String webPage) {
        BaseClassWeb.getDriver().get(webPage);
    }

    @When("^I verify the \"([^\"]*)\"$")
    public void iVerifyThe(String title) {
        softAssertion.assertTrue(BaseClassWeb.getDriver().getTitle().equals(title));
        softAssertion.assertAll();
    }

    @When("^I type text in to search box\"([^\"]*)\"$")
    public void iTypeTextInToSearchBox(String txt) {
        googlePageActions.searchBoxText(txt);
        googlePageActions.searchBoxText(Keys.ENTER);

    }

    @Then("^I check if the \"([^\"]*)\" is the first in the list$")
    public void iCheckIfTheIsTheFirstInTheList(String res) {
        softAssertion.assertEquals(googlePageActions.firstResultText(), res);
        softAssertion.assertAll();
    }

    @And("^I enter \"([^\"]*)\" name$")
    public void iEnterName(String userName) {
        aolPageActions.emailUserName(userName);
    }

    @And("^I click on next button$")
    public void iClickOnNextButton() throws InterruptedException {
        aolPageActions.nextButton();
        Thread.sleep(2000);
    }

    @And("^I enter \"([^\"]*)\"$")
    public void iEnter(String passwd) {
        aolPageActions.emailPasswd(passwd);
    }

    @And("^I verify email page$")
    public void iVerifyEmailPage() throws InterruptedException {
        softAssertion.assertEquals(aolPageActions.getTitle(), "AOL Mail");
        Thread.sleep(5000);
    }

    @When("^I create new email \"([^\"]*)\" with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iCreateNewEmailWithAnd(String to, String subject, String body) throws InterruptedException {

        aolPageActions.clickComposeBtn();
        if (aolPageActions.allowMesExist()) {
            aolPageActions.clickAllowed();
        }

        aolPageActions.typeTo(to);
        aolPageActions.typeSubject(subject);
        aolPageActions.typeBody(body);


    }

    @Given("^I am on AOL app$")
    public void iAmOnAOLApp() {
        softAssertion.assertTrue(true);
        softAssertion.assertAll();
    }


    @And("^I send email$")
    public void iSendEmail() {
        aolPageActions.clickSendBtn();
    }

    @Then("^I check if I received email$")
    public void iCheckIfIReceivedEmail() throws InterruptedException {
        while (aolPageActions.emptyEmail())
            Thread.sleep(1000);
        aolPageActions.mailReceiver();
    }

    @And("^I verify email sender \"([^\"]*)\" with \"([^\"]*)\" and delete\"$")
    public void iVerifyEmailSenderWithAndDelete(String sender, String subject) throws IOException {
        softAssertion.assertTrue(aolPageActions.mailSender().contains(sender));
        softAssertion.assertEquals(aolPageActions.mailSubject(), subject);
        softAssertion.assertAll();
        TakesScreenshot screenshot = (TakesScreenshot) BaseClass.getDriver();
        File saveFile = screenshot.getScreenshotAs(OutputType.FILE);
        File destFile = new File("target/Output/Email.png");
        FileUtils.copyFile(saveFile, destFile);
    }

    @And("^I delete email$")
    public void iDeleteEmail() {
        aolPageActions.clickDelete();
        softAssertion.assertTrue(aolPageActions.emptyEmail());
        softAssertion.assertAll();

    }


    @Given("^I am login to AOL mail with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iAmLoginToAOLMailWithAnd(String user, String passwd) {
        try {
            aolPageActions.emailUserName(user);
            aolPageActions.nextButton();
            aolPageActions.emailPasswd(passwd);
            aolPageActions.nextButton();
        } catch (NoSuchElementException ignored) {

        }
    }
}
