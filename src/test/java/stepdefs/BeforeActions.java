package stepdefs;


import cucumber.api.java.Before;
import org.testng.Reporter;
import utils.BaseClass;
import utils.BaseClassWeb;

import java.io.IOException;

public class BeforeActions {

    @Before
    public void setUp() throws IOException {
        String env = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("env");


        if (env.equals("Chrome")) {
            BaseClassWeb.setDriver();
        } else
            BaseClass.setDriver();

    }
}
