@Web
Feature: Testing Google.com

  Scenario Outline: Testing Google.com to verify that the web page loads correctly
    Given I navigate to "<website>"
    When I verify the "<title>"

    Examples:
      | website            | title  |
      | https://google.com | Google |

  Scenario Outline: Testing Google.com to verify that the web page loads correctly
    Given I navigate to "<website>"
    When I verify the "<title>"

    Examples:
      | website            | title      |
      | https://google.com | GoogleFail |

  Scenario Outline: Testing Google.com search for “mobile integration workgroup” and verify first result.
    Given I navigate to "<website>"
    When I type text in to search box"<searchText>"
    Then I check if the "<result>" is the first in the list

    Examples:
      | website            | searchText                   | result              |
      | https://google.com | mobile integration workgroup | https://miwtech.com |


