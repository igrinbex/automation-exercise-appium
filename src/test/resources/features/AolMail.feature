@App
Feature: Testing AOL Mail App

  Scenario Outline: Testing aol.com email
    Given I am login to AOL mail with "<user>" and "<password>"
    When I create new email "<to>" with "<subject>" and "<body>"
    And I send email
    Then I check if I received email
    And I verify email sender "Igor Gr" with "<subject>" and delete"
    And I delete email

    Examples:
      | user    | password | to              | subject         | body              |
      | igor_gr | !Q2w3e4r | igor_gr@aol.com | Automation Test | AOL new mail test |