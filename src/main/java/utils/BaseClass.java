package utils;


import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseClass {
    private final static int TIMEOUT = 10;
    static WebDriver driver;
    private static BaseClass baseDriver;

    private BaseClass() throws IOException {
        InputStream input = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/env.properties");
        Properties prop = new Properties();
        prop.load(input);
        URL url = new URL(prop.getProperty("remoteURL"));
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", prop.getProperty("platform.name"));
        caps.setCapability("platformVersion", prop.getProperty("platform.version"));
        caps.setCapability("deviceName", prop.getProperty("device.name"));
        caps.setCapability("uuid", prop.getProperty("uuid"));
        caps.setCapability("appPackage", prop.getProperty("app.package"));
        caps.setCapability("appActivity", prop.getProperty("app.activity"));
        driver = new AndroidDriver<WebElement>(url, caps);

        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
    }

    public static WebDriver getDriver() {

        return driver;
    }

    public static void setDriver() throws IOException {

        if (baseDriver == null) {
            baseDriver = new BaseClass();
        }
    }

    public static void setWebDriver() throws IOException {

        if (baseDriver == null) {
            baseDriver = new BaseClass();
        }
    }

    public static void tearDown() {

        if (driver != null) {
            driver.quit();
        }

        baseDriver = null;
    }
}
