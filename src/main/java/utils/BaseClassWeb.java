package utils;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseClassWeb {
    private final static int TIMEOUT = 10;
    private final static int PAGE_LOAD_TIMEOUT = 50;
    static RemoteWebDriver driver;
    private static BaseClassWeb baseDriver;
    private static WebDriverWait waitDriver;


    private BaseClassWeb() throws IOException {
        InputStream input = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/env.properties");
        Properties prop = new Properties();
        prop.load(input);
        URL url = new URL(prop.getProperty("remoteURL"));
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", prop.getProperty("platform.name"));
        caps.setCapability("platformVersion", prop.getProperty("platform.version"));
        caps.setCapability("deviceName", prop.getProperty("device.name"));
        caps.setCapability("uuid", prop.getProperty("uuid"));
        caps.setCapability("browserName", prop.getProperty("browser.name"));
        driver = new RemoteWebDriver(url, caps);

        waitDriver = new WebDriverWait(driver, TIMEOUT);
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
    }

    public static RemoteWebDriver getDriver() {

        return driver;
    }

    public static void setDriver() throws IOException {

        if (baseDriver == null) {
            baseDriver = new BaseClassWeb();
        }
    }

    public static void tearDown() {

        if (driver != null) {
            driver.quit();
        }

        baseDriver = null;
    }

}
