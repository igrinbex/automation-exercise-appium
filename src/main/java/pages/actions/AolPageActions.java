package pages.actions;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;
import pages.locators.AolPageLocators;
import utils.BaseClass;

public class AolPageActions {
    private AolPageLocators aolPageLocators;

    public AolPageActions() {

        this.aolPageLocators = new AolPageLocators();
        PageFactory.initElements(BaseClass.getDriver(), aolPageLocators);
    }

    public void emailUserNameExist() {
        aolPageLocators.aolMailUser.click();
    }

    public void emailUserName(String username) {
        aolPageLocators.aolMailUser.sendKeys(username);
    }

    public void emailPasswd(String passwd) {
        aolPageLocators.aolMailPasswd.sendKeys(passwd);
    }

    public void nextButton() {
        aolPageLocators.aolMailNextButton.click();
    }

    public String getTitle() {
        return BaseClass.getDriver().getTitle();
    }

    public void clickComposeBtn() {

        aolPageLocators.aolMailNewEmail.click();
    }

    public void typeTo(String to) throws InterruptedException {
        Thread.sleep(500);
        aolPageLocators.aolMailTo.sendKeys(to);
    }

    public void clickAllowed() {
        aolPageLocators.aolMailAllowContacts.click();
    }

    public boolean allowMesExist() {
        try {
            if (aolPageLocators.aolMailAllowMess.getSize() != null)
                return true;
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public void typeSubject(String subject) {

        aolPageLocators.aolMailSubject.sendKeys(subject);
    }

    public void typeBody(String body) {
        aolPageLocators.aolMailBody.sendKeys(body);
    }

    public void clickSendBtn() {
        aolPageLocators.aolMailSendButton.click();
    }

    public boolean emptyEmail() {
        try {
            if (aolPageLocators.aolMailEmptyMail.getSize() != null)
                return true;
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public void mailReceiver() {
        aolPageLocators.aolMailReceivedMail.click();
    }

    public String mailSender() {
        return aolPageLocators.aolMailVerifyFrom.getText();
    }

    public String mailSubject() {
        return aolPageLocators.aolMailSubjectMail.getText();
    }

    public void clickDelete() {
        aolPageLocators.deleteButton.click();
    }
}
