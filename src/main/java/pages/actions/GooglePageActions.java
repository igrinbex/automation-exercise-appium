package pages.actions;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import pages.locators.GooglePageLocators;
import utils.BaseClassWeb;

public class GooglePageActions {
    private GooglePageLocators googlePageLocators;

    public GooglePageActions() {
        this.googlePageLocators = new GooglePageLocators();
        PageFactory.initElements(BaseClassWeb.getDriver(), googlePageLocators);
    }

    public void searchBoxText(Keys enter) {
        googlePageLocators.googleSearchBox.sendKeys(enter);
    }

    public void searchBoxText(String text) {
        googlePageLocators.googleSearchBox.sendKeys(text);
    }

    public String firstResultText() {
        return googlePageLocators.googleFirstSearchResult.getText();
    }
}
