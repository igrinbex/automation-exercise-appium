package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AolPageLocators {


    @FindBy(how = How.ID, using = "//android.widget.EditText[@resource-id = 'login-username']")
    public WebElement aolMailUser;

    @FindBy(how = How.ID, using = "//android.widget.EditText[@resource-id = 'login-passwd']")
    public WebElement aolMailPasswd;

    @FindBy(how = How.ID, using = "//android.widget.Button[@resource-id = 'login-signin']")
    public WebElement aolMailNextButton;

    @FindBy(how = How.XPATH, using = "//android.widget.ImageButton[@resource-id = 'com.aol.mobile.aolapp:id/mail_fab']")
    public WebElement aolMailNewEmail;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout")
    public WebElement aolMailAllowMess;

    @FindBy(how = How.XPATH, using = "//android.widget.MultiAutoCompleteTextView[contains(@resource-id, 'com.aol.mobile.aolapp:id/email_field') and @content-desc='Enter recipients']")
    public WebElement aolMailTo;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]\n")
    public WebElement aolMailAllowContacts;

    @FindBy(how = How.XPATH, using = "//android.widget.EditText[@content-desc='Enter the subject']")
    public WebElement aolMailSubject;

    @FindBy(how = How.XPATH, using = "(//android.widget.EditText[@content-desc='Enter message body.'])[1]")
    public WebElement aolMailBody;

    @FindBy(how = How.ID, using = "com.aol.mobile.aolapp:id/menu_send_message")
    public WebElement aolMailSendButton;

    @FindBy(how = How.ID, using = "com.aol.mobile.aolapp:id/empty_view_text")
    public WebElement aolMailEmptyMail;

    @FindBy(how = How.ID, using = "com.aol.mobile.aolapp:id/message_list_cell_body")
    public WebElement aolMailReceivedMail;

    @FindBy(how = How.ID, using = "com.aol.mobile.aolapp:id/message_collapsed_header_from_text")
    public WebElement aolMailVerifyFrom;

    @FindBy(how = How.ID, using = "com.aol.mobile.aolapp:id/message_subject")
    public WebElement aolMailSubjectMail;

    @FindBy(how = How.ID, using = "com.aol.mobile.aolapp:id/menu_action_delete")
    public WebElement deleteButton;


    @FindBy(how = How.XPATH, using = "com.aol.mobile.aolapp:id/message_list_checkbox")
    public WebElement aolMailDeleteMail;
}