package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class GooglePageLocators {
    @FindBy(how = How.NAME, using = "q")
    public WebElement googleSearchBox;

    @FindBy(how = How.ID, using = "rso")
    public WebElement googleFirstSearchResult;
}
